package com.model;

import javax.persistence.Column;
import javax.persistence.Entity;
import javax.persistence.GeneratedValue;
import javax.persistence.GenerationType;
import javax.persistence.Id;
import javax.persistence.Table;

@Entity
@Table(name = "medicines")
public class Medicine {
	
	@Id
	@GeneratedValue
	private int med_id;
	
	@Column(name = "drugName")
	private String drugName;
	@Column(name = "stock")
	private String stock;
	
	public Medicine() {
		
	}
	public Medicine(String drugName, String stock) {
		super();
		this.drugName = drugName;
		this.stock = stock;
	}
	public int getId() {
		return med_id;
	}
	public void setId(int id) {
		this.med_id = id;
	}
	public String getDrugName() {
		return drugName;
	}
	public void setDrugName(String drugName) {
		this.drugName = drugName;
	}
	public String getStock() {
		return stock;
	}
	public void setStock(String stock) {
		this.stock = stock;
	}
	

}
