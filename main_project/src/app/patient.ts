export class Patient {
    id: number;
    name: string;
    age: string;
    blood: string;
    problem: string;
    prescription: string;
    dose: string;
    fees: string;
    urgency: string;
}
